#ifndef MP_CLASS
#define MP_CLASS

class MP_Class
{
public:
	MP_Class();
	double calculate(double data[], double er, double fr, double etarget, double ftarget, double Eorigin, double Etarget, double e, double Ro);

protected:

private:
	double a1;
	double a2;
	double eo;
	double fo;
	double b;
	double zeta;
	double R;
	double eeq;
	double feq;
	double f;
};

#endif



