#ifndef CYCLE_4_CLASS
#define CYCLE_4_CLASS

#include <math.h>
#include <algorithm>
#include "MP_Class.h"
#include "tension_Class.h"
#include "compression_DM_Class.h"

class Cycle_4_Class
{
public:
    Cycle_4_Class();
    double calculate(double emin, double  fmin, double  emax, double  fmax, double  data[], double  eorigin_T, double  eyc, double  fyc, double  estar, double  fstar, double  flstar, double  er, double  fr, double  Df, double  Zd, double* f_temp, double* f1_temp, double  e);

protected:

private:
    double etarget, offset, fr_t, fr_c, Eorigin, Etarget_2, etarget_temp, ftarget, Ro, f, Eb, ftarget_temp, etarget_temp1, Etarget;
    double ftarget_temp1, etarget_temp2, ftarget_temp2, offset_strain, envelope_stress;

    MP_Class MP;
    tension_Class tension;
    compression_DM_Class compression_DM;
};

#endif
