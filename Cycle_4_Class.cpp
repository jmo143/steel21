#include "Cycle_4_Class.h"

Cycle_4_Class::Cycle_4_Class()
{

}

double Cycle_4_Class::calculate(double emin, double  fmin, double  emax, double  fmax, double  data[], double  eorigin_T, double  eyc, double  fyc, double  estar, double  fstar, double  flstar, double  er, double  fr, double  Df, double  Zd, double* f_temp, double* f1_temp, double  e)
{
    //Unloading from Compression Envelope
    if (emax == 0 && fmax == 0) { // no tension has been applied so assume stiffness as 0.02Es, should be changed later
        etarget = data[6];
        offset = etarget - eorigin_T;
        fr_t = tension.calculate(data, fabs(offset));
        fr_c = compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(offset));
        Eorigin = data[2] * pow(fr_c / fr_t, 1.8);// Modified Dhakaland Maekawa
        Etarget_2 = 2000; // Target stiffness
        etarget_temp = etarget - ((-fr / Eorigin) + er);
        ftarget = (tension.calculate(data, etarget_temp)) * (1 - (Df * Zd));
        Ro = 20;
        f = MP.calculate(data, er, fr, etarget, ftarget, Eorigin, Etarget, e, Ro);
    }
    else {
        etarget = emax;
        // etarget_temp = etarget;
        offset = etarget - eorigin_T;
        fr_t = tension.calculate(data, abs(offset));
        fr_c = compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, abs(offset));
        Eorigin = data[2] * pow(fr_c / fr_t, 1.8);// Modified Dhakaland Maekawa
        etarget_temp = etarget - ((-fr / Eorigin) + er);
        ftarget = (tension.calculate(data, etarget_temp)) * (1 - (Df * Zd));
        if (ftarget < 0.4 * data[1]) {
            ftarget = 0.4 * data[1];
        }
        if (abs(fr) < 0.40 * data[1] && data[8] > 6) {
            offset = etarget - eorigin_T;
            fr_t = tension.calculate(data, fabs(offset));
            fr_c = compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(offset));
            Eb = data[2] * pow(fr_c / fr_t, 1.8); // Dhakaland Maekawa
            ftarget_temp = -fr;
            etarget_temp1 = er + (1.5 * (ftarget_temp - fr) / Eb);
            if (e <= etarget_temp1) {
                offset = etarget - eorigin_T;
                fr_t = tension.calculate(data, fabs(offset));
                fr_c = compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(offset));
                Eorigin = data[2] * pow(fr_c / fr_t, 1.8); // Dhakaland Maekawa
                Etarget = 0.05 * Eorigin;
                Ro = 20;
                f = MP.calculate(data, er, fr, etarget_temp1, ftarget_temp, Eorigin, Etarget, e, Ro);
                *f_temp = f;
            }
            else if (e > etarget_temp1 && e <= etarget) {
                ftarget_temp = *f_temp;
                offset = etarget - eorigin_T;
                fr_t = tension.calculate(data, fabs(offset));
                Etarget = 1.0 * data[1] * ((2.2 * data[8]));
                // if Etarget > 0.05 * data(2)
                    // Etarget = 0.05 * data(2);
                // elseif Etarget < 0.02 * data(2)
                    // Etarget = 0.02 * data(2);
                // end
                Eorigin = 0.05 * Etarget;
                Ro = 20;
                f = MP.calculate(data, etarget_temp1, ftarget_temp, etarget, ftarget, Eorigin, Etarget, e, Ro);
                *f1_temp = f;
            }
            else {
                ftarget_temp1 = *f1_temp;
                Eorigin = 2000;
                // Eorigin = 5000;
                Etarget = 1;
                Ro = 20;
                etarget_temp2 = data[4] - etarget_temp1;
                if (etarget_temp2 > 0.25) {
                    etarget_temp2 = 0.25;
                }
                ftarget_temp2 = (tension.calculate(data, etarget_temp2));
                f = MP.calculate(data, etarget, ftarget_temp1, etarget_temp2, ftarget_temp2, Eorigin, Etarget, e, Ro);
            }
        }
        else {
            Etarget = 2000;// strain hardening stiffness
            Ro = 20;
            f = MP.calculate(data, er, fr, etarget, ftarget, Eorigin, Etarget, e, Ro);
            if (e > etarget) {
                offset_strain = e + abs(eorigin_T);
                envelope_stress = tension.calculate(data, offset_strain);
                f = std::min(f, envelope_stress);
            }
        }
    }
    return f;
}


