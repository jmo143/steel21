#ifndef CYCLE_3_CLASS
#define CYCLE_3_CLASS

#include <math.h>
#include "MP_Class.h"
#include "compression_DM_Class.h"

class Cycle_3_Class
{
public:
    Cycle_3_Class();
    double calculate(double emin, double fmin, double  data[], double  emax, double  fmax, double  eyc, double  fyc, double  estar, double  fstar, double  flstar, double  eorigin_C, double  er, double  fr, double  e);

protected:

private:
    double Ro;
    double gamma;
    double fyc_mod;
    double etarget;
    double etarget_temp;
    double eyc_mod;
    double ftarget;
    double Eorigin;
    double Etarget;
    double f;
 
    MP_Class MP;
    compression_DM_Class compression_DM;
};

#endif

