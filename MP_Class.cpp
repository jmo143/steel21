#include <math.h>
#include "MP_Class.h"

MP_Class::MP_Class()
{
	double a1 = 0;
	double a2 = 0;
	double eo = 0;
	double fo = 0;
	double b = 0;
	double zeta = 0;
	double R = 0;
	double eeq = 0;
	double feq = 0;
	double f = 0;
}

double MP_Class::calculate(double data[], double er, double fr, double etarget, double ftarget, double Eorigin, double Etarget, double e, double Ro) {

	//This function develops an unloading curver from compression envelope to the tension envelope
	a1 = 18.5;
	a2 = 0.15;
	eo = (ftarget - fr - (Etarget * etarget) + (Eorigin * er)) / (Eorigin - Etarget);
	fo = fr + (Eorigin * (eo - er));
	b = Etarget / Eorigin;// M - P Parameter
	zeta = fabs((etarget - eo) / data[5]);
	R = Ro - (a1 * zeta / (a2 + zeta));
	if (R <= 0) {
		R = 0.01;
	}
	eeq = (e - er) / (eo - er);
	feq = (b * eeq) + (((1 - b) * eeq) / pow(1 + pow(eeq,R), (1 / R)));
	f = (feq * (fo - fr)) + fr;
	return f;
}
