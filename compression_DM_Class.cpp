#include "compression_DM_Class.h"

compression_DM_Class::compression_DM_Class() {

}
double compression_DM_Class::calculate(double data[], double eyc, double fyc, double estar, double fstar, double flstar, double e){
    if (e >= 0 && e <= data[5]) {
        fc = data[2] * e;
    }
    else if (e > data[5] && e <= estar) {
        fl = tension.calculate(data, e);
        fc = fl * (1 - ((1 - (fstar / flstar)) * ((e - data[5]) / (estar - data[5]))));
    }
    else {
        fc = fstar - (0.02 * data[2] * (e - estar));
        if (fc < 0.2 * data[1]) {
            fc = 0.2 * data[1];
        }
    }
    return fc;
}
