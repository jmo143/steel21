/* ****************************************************************** **
**    OpenSees - Open System for Earthquake Engineering Simulation    **
**          Pacific Earthquake Engineering Research Center            **
**                                                                    **
**                                                                    **
** (C) Copyright 1999, The Regents of the University of California    **
** All Rights Reserved.                                               **
**                                                                    **
** Commercial use of this program without express permission of the   **
** University of California, Berkeley, is strictly prohibited.  See   **
** file 'COPYRIGHT'  in main directory for information on usage and   **
** redistribution,  and for a DISCLAIMER OF ALL WARRANTIES.           **
**                                                                    **
** Developed by:                                                      **
**   Frank McKenna (fmckenna@ce.berkeley.edu)                         **
**   Gregory L. Fenves (fenves@ce.berkeley.edu)                       **
**   Filip C. Filippou (filippou@ce.berkeley.edu)                     **
**                                                                    **
** ****************************************************************** */
                                                                        
// $Revision: 1.7 $
// $Date: 2009/03/23 23:17:04 $
// $Source: /usr/local/cvs/OpenSees/PACKAGES/NewMaterial/cpp/Steel21.cpp,v $
                                                                        
// Written: fmk 
//
// Description: This file contains the class implementation for 
// ElasticMaterial. 
//
// What: "@(#) Steel21.C, revA"
#include <elementAPI.h>
#include <iostream>
#include <algorithm>
#include "Steel21.h"

#include <Vector.h>
#include <Channel.h>
#include <math.h>
#include <float.h>





static int numSteel21 = 0;

 void *
OPS_Steel21()
{
  // print out some KUDO's
  if (numSteel21 == 0) {
    opserr << "Steel21 unaxial material - Written by fmk UC Berkeley Copyright 2008 - Use at your Own Peril\n";
    numSteel21 =1;
  }

  // Pointer to a uniaxial material that will be returned
  UniaxialMaterial *theMaterial = 0;

  //
  // parse the input line for the material parameters
  //

  int    iData[1];
  double dData[6];
  int numData;
  numData = 1;
  if (OPS_GetIntInput(&numData, iData) != 0) {
    opserr << "WARNING invalid uniaxialMaterial ElasticPP tag" << endln;
    return 0;
  }

  numData = 6;
  if (OPS_GetDoubleInput(&numData, dData) != 0) {
    opserr << "WARNING invalid E & ep\n";
    return 0;	
  }

  // 
  // create a new material
  //

  theMaterial = new Steel21(iData[0], dData[0], dData[1], dData[2], dData[3], dData[4], dData[5]);


  if (theMaterial == 0) {
    opserr << "WARNING could not create uniaxialMaterial of type Steel21\n";
    return 0;
  }

  // return the material
  return theMaterial;
}


Steel21::Steel21(int tag, double fyt_in, double E_in, double fut_in, double eut_in, double Esh_in, double SR_in) :
UniaxialMaterial(tag, 0)
 //,ezero(0.0), E(e), ep(0.0),
 //trialStrain(0.0), trialStress(0.0), trialTangent(E),
 //commitStrain(0.0), commitStress(0.0), commitTangent(E)
{
  SF = 0;
  data[0] = 0; //No use
  //data[1] = 530; //Tensile yield stress in MPa 300/530
  //data[2] = 200000; //Modulus of elasticity in MPa
  //data[3] = 611.3; //Ultimate tensile stress in MPa 428.6/611.3
  //data[4] = 0.14; //Ultimate tensile strain 0.25/0.14
  //data[5] = data[1] / data[2]; //Calculate yield strain assuming elastic response
  //data[6] = 8.5 * data[5]; //Yield plateau esh=18/8.5*ey
  //data[7] = 3500; //Strain hardening modulus Esh=3500
  //data[8] = 15; //Slenderness ratio of reinforcing bars
  //data[9] = data[8] * sqrt(data[1] / 100); //Calculating the buckling parameter
  // ////////////////////////////////
  data[1] = fyt_in; //Tensile yield stress in MPa 300/530
  data[2] = E_in; //Modulus of elasticity in MPa
  data[3] = fut_in; //Ultimate tensile stress in MPa 428.6/611.3
  data[4] = eut_in; //Ultimate tensile strain 0.25/0.14
  data[5] = data[1] / data[2]; //Calculate yield strain assuming elastic response
  data[6] = 8.5 * data[5]; //Yield plateau esh=18/8.5*ey
  data[7] = Esh_in; //Strain hardening modulus Esh=3500
  data[8] = SR_in; //Slenderness ratio of reinforcing bars
  data[9] = data[8] * sqrt(data[1] / 100); //Calculating the buckling parameter

  alpha = 55 - (2.3 * data[9]); //Transition strain estimation
  eyc = -data[5]; //Yield strain in compression equal to negative of tensile strain
  fyc = -data[1]; //Yield stress in compression equal to negative of compressive strain
  k = 0; //counter defining the type of path if k=0 monotonic, if k=3 unloading from tension, if k=4 unloading from compression
       //Material parameters to develop monotonic compression  response

  if (alpha >= 7)
  {
      estar = alpha * data[5];
  }
  else
  {
      estar = 7 * data[5];
  }
  //Estimating the alpha parameter
  alpha_parameter = 0.75 + ((data[4] - data[6]) / (300 * data[5]));
  Ualpha_parameter = data[3] / (1.5 * data[1]);
  if (alpha_parameter >= Ualpha_parameter)
  {
      alpha_parameter = Ualpha_parameter;
      if (alpha_parameter > 1)
      {
          alpha_parameter = 1;
      }
      else if (alpha_parameter < 0.75)
      {
          alpha_parameter = 0.75;
      }
  }

  //Estimating stress corresponding to intermediate point
  beta = alpha_parameter * (1.1 - (0.016 * data[9]));

  if (estar > data[5] && estar <= data[6])
  {
      flstar = tension.calculate(data, estar);
  }
  else
  {
      flstar = tension.calculate(data, estar);
  }

  if (beta >= 0.2 * data[1] / flstar)
  {
      fstar = beta * flstar;
  }
  else
  {
      fstar = 0.2 * data[1];
  }

}

Steel21::Steel21()
:UniaxialMaterial(0, 0),
 fyp(0.0), fyn(0.0), ezero(0.0), E(0.0), ep(0.0),
 trialStrain(0.0), trialStress(0.0), trialTangent(E),
 commitStrain(0.0), commitStress(0.0), commitTangent(E)
{

}

Steel21::~Steel21()
{
  // does nothing
}

int 
Steel21::setTrialStrain(double strain, double strainRate)
{
    if (fabs(trialStrain - strain) < DBL_EPSILON) //如果trialstrain - strain = 0 返回0
      return 0;

    /// //////////////////////////

    e = strain;
    delta_1 = e - eP;
    if (delta_1 < 0 && delta_2>0) { // unloading from tension
        k = 3;
        er = eP;
        fr = trialstress;
        eorigin_C = er - data[5];// Set off - set of going to another side
        if (emax <= eP) { // use a temp strain and chech if it is maximum
            emax = eP;
            fmax = trialstress;
        }
    }
    else if (delta_1 > 0 && delta_2 < 0) { // unloading from compression
        k = 4;
        er = eP;
        fr = trialstress;
        eorigin_T = er - (-data[5]);// Set off - set of going to another side
        if (emin >= eP) {
            emin = eP;
            fmin = trialstress;
        }
    }

    if (SF == 0) { // Initialising the parameters
        A = e;
        B = 0;
        C = 0;
        D = 0;
        SF = 1;
        PCC = 0;
    }

    if ((delta_1 < 0 && delta_2>0)  || (delta_1 > 0 && delta_2 < 0)) {
        if (RIF == 0) { // first time changing sign of gradient
            B = eP;
            Y = fabs(B - A); 
            RIF = 1;
        }
        else {
            if (PCC == 1) {
                D = eP;
                X = fabs(D - C);// strain range
            }
            else {
                C = eP;
                X = fabs(C - B);
            }
            if (X < Y) {
                PCC = PCC + 1;
                if (PCC == 1) {
                    Y = fabs(C - B);
                }
                else if (PCC == 2) {
                    Fat_Damage.calculate(&DL, &Zd, data, X);
                    Df = Df + (2 / DL);
                    DL = 0;
                    Nf = Nf + 2;
                    SR1 = X;
                    NC1 = X;
                    D = 0;
                    C = 0;
                    Y = fabs(B - A);
                    PCC = 0;
                }
            }
            else {
                if (PCC == 1) {
                    Fat_Damage.calculate(&DL, &Zd, data, Y);
                    Df = Df + (2 / DL);
                    DL = 0;
                    Nf = Nf + 2;
                    SR1 = Y;
                    B = D;
                    C = 0;
                    D = 0;
                    Y = fabs(B - A);
                    PCC = 0;
                }
                else {
                    Fat_Damage.calculate(&DL, &Zd, data, Y);
                    Df = Df + (1 / DL);
                    DL = 0;
                    Nf = Nf + 1;
                    SR1 = Y;
                    NC1 = 0.5;
                    A = B;
                    B = C;
                    C = 0;
                    D = 0;
                    Y = X;
                    PCC = 0;
                }
            }
        }
    }
    if (Df > 1) {
        Df = 1;
    }
    if (k == 0) {
        if (delta_1 > 0) {
            f = Cycle_1.calculate(data, e);
        }
        else {
            f = compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(e));
        }
    }
    else if (k != 0 && k != 4) {
        f = Cycle_3.calculate(emin, fmin, data, emax, fmax, eyc, fyc, estar, fstar, flstar, eorigin_C, er, fr, e);
    }
    else if (k != 0 && k != 3) {
        f = Cycle_4.calculate(emin, fmin, emax, fmax, data, eorigin_T, eyc, fyc, estar, fstar, flstar, er, fr, Df, Zd, &f_temp, &f1_temp, e);
    }
    if (Df == 1) {
        f = 0 * data[1];
    }
    trialTangent = (f - fP) / (e - ep);

        // cout << flstar;
    return 0;
}

double 
Steel21::getStrain(void)
{
    return e;
}

double 
Steel21::getStress(void)
{
  return f;
}


double 
Steel21::getTangent(void)
{
  return trialTangent;
}

int 
Steel21::commitState(void)
{
    delta_2 = delta_1;
    eP = e;
    fP = f;
    emaxP = emax;
    fmaxP = fmax;
    eminP = emin;
    fminP = fmin;
    f_tempP = f_temp;
    f1_tempP = f1_temp;
    trialstrainP = trialstrain;
    trialstressP = trialstress;
    //delta_1P = delta_1;
    //delta_2P = delta_2;
    erP = er;
    frP = fr;
    DfP = Df;
    SFP = SF;
    PCCP = PCC;
    RIFP = RIF;
    NfP = Nf;
    eorigin_CP = eorigin_C;
    eorigin_TP = eorigin_T;
    AP = A;
    BP = B;
    CP = C;
    DP = D;
    XP = X;
    YP = Y;
    DLP = DL;
    ZdP = Zd;
    SR1P = SR1;
    NC1P = NC1;

    return 0;
}	


int 
Steel21::revertToLastCommit(void)
{
    delta_1 = delta_2;
    e = eP;
    f = fP;

    emax = emaxP;
    fmax = fmaxP;
    emin = eminP;
    fmin = fminP;
    f_temp = f_tempP;
    f1_temp = f1_tempP;
    trialstrain = trialstrainP;
    trialstress = trialstressP;
    //delta_1 = delta_1P;
    //delta_2 = delta_2P;
    er = erP;
    fr = frP;
    Df = DfP;
    SF = SFP;
    PCC = PCCP;
    RIF = RIFP;
    Nf = NfP;
    eorigin_C = eorigin_CP;
    eorigin_T = eorigin_TP;
    A = AP;
    B = BP;
    C = CP;
    D = DP;
    X = XP;
    Y = YP;
    DL = DLP;
    Zd = ZdP;
    SR1 = SR1P;
    NC1 = NC1P;

  return 0;
}


int 
Steel21::revertToStart(void)
{
    delta_1 = 0;
    delta_2 = 0;
    e = eP = 0;
    f = fP = 0;

    emax = emaxP = 0;
    fmax = fmaxP = 0;
    emin = eminP = 0;
    fmin = fminP = 0;
    f_temp = f_tempP = 0;
    f1_temp = f1_tempP = 0;
    trialstrain = trialstrainP = 0;
    trialstress = trialstressP = 0;
    //delta_1 = delta_1P= 0;
    //delta_2 = delta_2P= 0;
    er = erP = 0;
    fr = frP = 0;
    Df = DfP = 0;
    SF = SFP = 0;
    PCC = PCCP = 0;
    RIF = RIFP = 0;
    Nf = NfP = 0;
    eorigin_C = eorigin_CP = 0;
    eorigin_T = eorigin_TP = 0;
    A = AP = 0;
    B = BP = 0;
    C = CP = 0;
    D = DP = 0;
    X = XP = 0;
    Y = YP = 0;
    DL = DLP = 0;
    Zd = ZdP = 0;
    SR1 = SR1P = 0;
    NC1 = NC1P = 0;

  return 0;
}


UniaxialMaterial *
Steel21::getCopy(void)
{
    Steel21 *theCopy =
    new Steel21(this->getTag(),data[1], data[2], data[3], data[4], data[7], data[8]);
  theCopy->ep = this->ep;
  
  return theCopy;
}


int 
Steel21::sendSelf(int cTag, Channel &theChannel)
{
  int res = 0;
  static Vector data(9);
  data(0) = this->getTag();
  data(1) = ep;
  data(2) = E;
  data(3) = ezero;
  data(4) = fyp;
  data(5) = fyn;
  data(6) = commitStrain;
  data(7) = commitStress;
  data(8) = commitTangent;

  res = theChannel.sendVector(this->getDbTag(), cTag, data);
  res = -1;
  if (res < 0) 
    opserr << "Steel21::sendSelf() - failed to send data\n";

  return res;
}

int 
Steel21::recvSelf(int cTag, Channel &theChannel,
				 FEM_ObjectBroker &theBroker)
{
  int res = 0;
  static Vector data(9);
  res = theChannel.recvVector(this->getDbTag(), cTag, data);
  res = -1;
  if (res < 0) 
    opserr << "Steel21::recvSelf() - failed to recv data\n";
  else {
    this->setTag(data(0));
    ep    = data(1);
    E     = data(2);
    ezero = data(3);
    fyp   = data(4);
    fyn   = data(5);  
    commitStrain=data(6);
    commitStress=data(7);
    commitTangent=data(8);
    trialStrain = commitStrain;
    trialTangent = commitTangent;
    trialStress = commitStress;
  }

  return res;
}

void 
Steel21::Print(OPS_Stream &s, int flag)
{
  s << "Steel21 tag: " << this->getTag() << endln;
  s << "  E: " << E << endln;
  s << "  ep: " << ep << endln;
  s << "  stress: " << trialStress << " tangent: " << trialTangent << endln;
}