/* ****************************************************************** **
**    OpenSees - Open System for Earthquake Engineering Simulation    **
**          Pacific Earthquake Engineering Research Center            **
**                                                                    **
**                                                                    **
** (C) Copyright 1999, The Regents of the University of California    **
** All Rights Reserved.                                               **
**                                                                    **
** Commercial use of this program without express permission of the   **
** University of California, Berkeley, is strictly prohibited.  See   **
** file 'COPYRIGHT'  in main directory for information on usage and   **
** redistribution,  and for a DISCLAIMER OF ALL WARRANTIES.           **
**                                                                    **
** Developed by:                                                      **
**   Frank McKenna (fmckenna@ce.berkeley.edu)                         **
**   Gregory L. Fenves (fenves@ce.berkeley.edu)                       **
**   Filip C. Filippou (filippou@ce.berkeley.edu)                     **
**                                                                    **
** ****************************************************************** */
                                                                        
// $Revision: 1.1 $
// $Date: 2008/12/09 20:00:16 $
// $Source: /usr/local/cvs/OpenSees/PACKAGES/NewMaterial/cpp/ElasticPPcpp.h,v $
                                                                        
#ifndef Steel21_h
#define Steel21_h

// Written: fmk 
//
// Description: This file contains the class definition for 
// ElasticPPcpp. ElasticPPcpp provides the abstraction
// of an elastic perfectly plastic uniaxial material, 
//
// What: "@(#) ElasticPPcpp.h, revA"

#include <UniaxialMaterial.h>
#include "Cycle_1_Class.h"
#include "Cycle_3_Class.h"
#include "Cycle_4_Class.h"
#include "tension_Class.h"
#include "Fat_Damage_Class.h"
#include "compression_DM_Class.h"

class Steel21 : public UniaxialMaterial
{
  public:
      Steel21(int tag, double fyt_in, double E_in, double fut_in, double eut_in, double Esh_in, double SR_in);
      Steel21();

    ~Steel21();

    int setTrialStrain(double strain, double strainRate = 0.0); 
    double getStrain(void);          
    double getStress(void);
    double getTangent(void);

    double getInitialTangent(void) {return E;};

    int commitState(void);
    int revertToLastCommit(void);    
    int revertToStart(void);    

    UniaxialMaterial *getCopy(void);
    
    int sendSelf(int commitTag, Channel &theChannel);  
    int recvSelf(int commitTag, Channel &theChannel, 
		 FEM_ObjectBroker &theBroker);    
    
    void Print(OPS_Stream &s, int flag =0);


  protected:
    
  private:
    double fyp ,fyn = 0;	// positive and negative yield stress
    double ezero = 0;	// initial strain
    double E = 0;		// elastic modulus
    double ep = 0;		// plastic strain at last commit

    double trialStrain = 0;	// trial strain
    double trialStress = 0;      // current trial stress
    double trialTangent = 0;     // current trial tangent
    double commitStrain = 0;     // last committed strain
    double commitStress = 0;     // last committed stress
    double commitTangent = 0;    // last committed  tangent

// cutting line!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    double SF = 0; // state define (SF = 0 [first time running] SF = 1 [not first time/ignore])
    double alpha = 0;
    double eyc = 0;
    double fyc = 0;
    double data[10];
    int k = 0;
    //double ft;
    double flstar = 0;
    double estar = 0;
    double alpha_parameter = 0;
    double Ualpha_parameter = 0;
    double beta = 0;
    double fstar = 0;

    double emax = 0; //max tensile strain ever achieved(to be updated)
    double fmax = 0; //tensile stress corresponding to emax
    double emin = 0; //max compressive strain achieved(to be updated)
    double fmin = 0; //compressive stress corresponding to emax
    //double ee = 0; //Parameter storing immediately preceding strain value
    double f_temp = 0;
    double f1_temp = 0;
    double trialstrain = 0;
    double trialstress = 0;
    double delta_1 = 0; //Current strain change (current strain- last commit strain) (this step)
    double delta_2 = 0; //Old strain change (current strain- last commit strain) (this step)
    double er = 0;
    double fr = 0;
    double Df = 0; //Damage
    double PCC = 0;
    double RIF = 0;
    double Nf = 0;
    double e = 0; // trail strain
    double eorigin_C = 0; // strain off-set of going to another side (from T to C)
    double eorigin_T = 0; // strain off-set of going to another side (from C to T) 
    double A = 0;
    double B = 0;
    double C = 0;
    double D = 0;
    double X = 0;
    double Y = 0;// abs(strain last commit - current strain) (strain difference between steps)
    double f = 0;// Current stress (output)

    double DL = 0;
    double Zd = 0;
    double SR1 = 0;
    double NC1 = 0;


    double emaxP = 0; //max tensile strain ever achieved(to be updated)
    double fmaxP = 0; //tensile stress corresponding to emax
    double eminP = 0; //max compressive strain achieved(to be updated)
    double fminP = 0; //compressive stress corresponding to emax
    double f_tempP = 0;
    double f1_tempP = 0;
    double trialstrainP = 0;
    double trialstressP = 0;
    double delta_1P = 0; //Current strain change (current strain- last commit strain) (this step)
    double delta_2P = 0; //Old strain change (current strain- last commit strain) (this step)
    double erP = 0;
    double frP = 0;
    double DfP = 0; //Damage
    double SFP = 0;
    double PCCP = 0;
    double RIFP = 0;
    double NfP = 0;
    double eorigin_CP = 0; // strain off-set of going to another side (from T to C)
    double eorigin_TP = 0; // strain off-set of going to another side (from C to T) 
    double AP=0;
    double BP=0;
    double CP=0;
    double DP=0;
    double XP=0;
    double YP = 0;// abs(strain last commit - current strain) (strain difference between steps)
    double eP = 0;		// plastic strain at last commit
    double fP = 0;
    double DLP = 0;
    double ZdP = 0;
    double SR1P = 0;
    double NC1P = 0;

    Cycle_1_Class Cycle_1;
    Cycle_3_Class Cycle_3;
    Cycle_4_Class Cycle_4;
    tension_Class tension;
    Fat_Damage_Class Fat_Damage;
    compression_DM_Class compression_DM;

};


#endif



