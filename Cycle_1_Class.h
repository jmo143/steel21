#ifndef CYCLE_1_CLASS
#define CYCLE_1_CLASS

#include "tension_Class.h"

class Cycle_1_Class
{
public:
	Cycle_1_Class();
	double calculate(double data[], double e);

protected:

private:
	double f;
	tension_Class tension;
};

#endif


