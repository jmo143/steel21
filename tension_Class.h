#ifndef TENSION_Class
#define TENSION_Class

#include <math.h>

class tension_Class
{
public:
	tension_Class();
	double calculate(double data[], double e);

protected:

private:
	double ft;
	double p;
};

#endif

