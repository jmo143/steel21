#include "Fat_Damage_Class.h"
Fat_Damage_Class::Fat_Damage_Class(){

}

void Fat_Damage_Class::calculate(double *DL, double *Zd, double data[], double X)
{

    if (data[9] < 10.39) {
        lmbda = 10.39;
    }
    else{
        lmbda = data[9];
    }
//Estimating the fatigue parameters
    beta = (-lmbda / 350) + 0.2;
    a = -((lmbda / 1200) + 0.441);
    b = -a;
//Estimating the damage
    *DL = pow(((X / 2) / beta) , (1 / a));
    *Zd = 1;
}



