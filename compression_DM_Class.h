#ifndef COMPRESSION_DM_CLASS
#define COMPRESSION_DM_CLASS

#include "tension_Class.h"

class compression_DM_Class
{
public:
	compression_DM_Class();
	double calculate(double data[], double eyc, double fyc, double estar, double fstar, double flstar, double e);

protected:

private:
	double fc;
	double fl;
	tension_Class tension;
};

#endif

