#include "Cycle_3_Class.h"

Cycle_3_Class::Cycle_3_Class()
{

}

double Cycle_3_Class::calculate(double emin, double fmin, double  data[], double  emax, double  fmax, double  eyc, double  fyc, double  estar, double  fstar, double  flstar, double  eorigin_C, double  er, double  fr, double  e) {
	Ro = 20;
	if (emin == 0 && fmin == 0) { // no compression has been applied so assume stiffness as(modified_fyc - flstar) / (eyc - eystar)
		gamma = 1.3 * pow(2.7182818284, -0.8 * data[9] * (emax - data[5]));// Stress redcution factor due to tensile stress history
		fyc_mod = gamma * fyc;
		if (fabs(fyc_mod) < fabs(0.2 * fyc)) {
			fyc_mod = 0.20 * fyc;
		}
		etarget = -estar;// as no compression has been applied, the target strain is the intermediate compressive strain as estimated earlier
		etarget_temp = etarget - eorigin_C;
		eyc_mod = eorigin_C - data[5];
		ftarget = -(compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(etarget_temp)));
		Eorigin = data[2] * (0.82 + (1 / (5.55 + (1000 * (emax - data[5])))));// Doddand Restrepo
		Etarget = (fyc_mod - ftarget) / (eyc_mod - etarget);// Target Stifness
		Ro = 20;// Constant parameter for MP cyclic curves
		f = MP.calculate(data, er, fr, etarget, ftarget, Eorigin, Etarget, e, Ro);
		if ((e <= 0) && (fabs(e) > fabs(etarget)) && (f > -0.2 * data[1])) {
			f = -0.2 * data[1];
		}
		else if (e <= 0 && f > -0.2 * data[1]) {
			f = -0.2 * data[1];
		}
	}
	else {
		gamma = 1.3 * pow(2.7182818284, -0.8 * data[9] * (emax - data[5]));
		fyc_mod = gamma * fyc;
		if (fabs(fyc_mod) < fabs(0.2 * fyc)) {
			fyc_mod = 0.2 * fyc;
		}
		eyc_mod = eorigin_C - data[5];
		etarget = emin;
		etarget_temp = etarget - eorigin_C;
		ftarget = -(compression_DM.calculate(data, eyc, fyc, estar, fstar, flstar, fabs(etarget_temp)));
		if (fabs(ftarget) < 0.2 * data[1]) {
			ftarget = 0.2 * fyc;
		}
		Eorigin = data[2] * (0.82 + (1 / (5.55 + (1000 * (emax - data[5])))));// Doddand Restrepo
		Etarget = (ftarget - fyc_mod) / (etarget - eyc_mod);
		Ro = 20;

		f = MP.calculate(data, er, fr, etarget, ftarget, Eorigin, Etarget, e, Ro);
		if ((e <= 0) && (fabs(e) > fabs(etarget)) && (f > -0.2 * data[1])) {
			f = -0.2 * data[1];
		}
		else if (e <= 0 && f > -0.2 * data[1]) {
			f = -0.2 * data[1];
		}
	}
	return f;
}
