void Fat_Damage(double* DL, double* Zd, double data[], double X);
#include <math.h>
#ifndef FAT_DAMAGE_CLASS
#define FAT_DAMAGE_CLASS



class Fat_Damage_Class
{
public:
	Fat_Damage_Class();
	void calculate(double* DL, double* Zd, double data[], double X);

protected:

private:
    double lmbda;
    double beta;
    double a;
    double b;
};

#endif



